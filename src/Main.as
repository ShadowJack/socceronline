/**
 * @author ShadowJack
 * @usage Главный класс, отвечающий заинициализацию основных управляющих
 * объектов, поключение к циррусу, работу с апи вконтакте, подключение к локальной группе
 * обработку событий на игровом поле - нажатие клавиш, управление физикой, обработка 
 * присылаемых от других игроков данных
 */

//TODO: оптимизировать обмен данными - уменьшить лаги
//TODO: добавить ограничение на перемещение игроков, пока мяч ещё не разведён
//TODO: добавить сервер, который будет содержать в себе все лобби, существующие в данный момент
//TODO: ожидать соперников(хотя бы одного) при заходе в группу

package 
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	import flash.ui.*;
	import flash.utils.Timer;
	import Math;
	import ru.shadowjack.utils.ConnectionManager;

	import ru.shadowjack.soccer.*;
	import vk.APIConnection;
	import vk.api.serialization.json.*;
	import vk.events.CustomEvent;
	import Allfield;
	import Menu;
	import MenuIconBtn;
	
	public class Main extends Sprite 
	{
		
		public var flashVars:Object;
		public var VK:APIConnection;
		public var player:PlayerObject = null;
		public var others:Object = {};
		public var ball:Ball = null;
		public var menuIcon:MenuIconBtnWrapper;
		public var scoreField:ScoreField;
		public var country:Object = null;
		public var region:Object = null;
		public var timer:TimerField = null;
		public var connector:ConnectionManager;
		public var sleep:Boolean = false;
		public var sleepCounter:int = 0;
		
		// DEBUG - текстовое поле для отладки
		public var console:TextField;
		
		public var menu:NewInteractiveMenu;
		public var allField:Allfield;
		public var bars:Array = [];
		
		private var _userName:String;
		private var _counter:int = 0;
		private var _groupsListTmp:Array = [];
		
		public function Main():void 
		{
			if(stage)
				init();
			else
				addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			if (e)
				removeEventListener(Event.ADDED_TO_STAGE, init);
			this.stage.scaleMode = StageScaleMode.NO_SCALE;
			flashVars = Object(stage.loaderInfo.parameters);
			if (!flashVars.api_id)
			{
				// Значения для тестирования локально:
				flashVars['api_id'] = 3658466;
				flashVars['viewer_id'] = 7983602;
				flashVars['sid'] = "1917adcbdcb4bfa026536bf6aab5d11f62a604d432e72b49164f5b4b6e9e72283d5c9c3c5cfee38e0f256";
				flashVars['secret'] = "eb3ba36ce8";
				flashVars['is_app_user'] = "1";
				// -- //
			}
			VK = new APIConnection(flashVars);
			VK.addEventListener(CustomEvent.APP_ADDED, onAppInstalled);
			// проверяем, установил ли пользователь приложение, если нет, то просим установить
			VK.api("users.isAppUser", { uid : flashVars['viewer_id'] }, function(result:Object):void
			{
				if (result != "1") // если пользователь не установил игру, то просим установить
				{
					//console.appendText("Запрос на установку приложения\n")
					VK.callMethod("showInstallBox");
				}
			},
			function(error:Object):void
			{
				trace(error);
			});
			// добавляем поле для игры
			allField = new Allfield();
			addChild(allField);
			// убираем с поля старую иконку меню и ставим новую
			allField.removeChild(allField.getChildAt(4));
			menuIcon = new MenuIconBtnWrapper();
			menuIcon.x = 760;
			menuIcon.y = 10;
			allField.addChild(menuIcon);
			
			// добавляем штанги на поле allField
			FieldObject.addBars(allField);
			
			// создаём игрока, но не добавляем на поле
			player = new PlayerObject(0, 0);
			
			// Открываем меню
			menu = new NewInteractiveMenu();
			menu.x = 0;
			menu.y = 0;
			addChild(menu);
			
			// добавляем текстовое поле для отладки
			
			console = new TextField();
			console.autoSize = TextFieldAutoSize.LEFT;
			console.border = true;
			console.multiline = true;
			console.type = TextFieldType.DYNAMIC;
			console.x = 50;
			console.y = 30;
			addChild(console);
			
			
			connector = new ConnectionManager(this);
		}
		
		/**
		 *  Метод обработки события установки приложения.
		 *  @usage Выполняем серию запросов к апи вконтакте для того,
		 *  		   чтобы получить регион пользователя и сохранить его в серверной переменной.
		 **/
		private function onAppInstalled(e:CustomEvent):void 
		{
			// выводим меню с запросом ввести ник пользователя
			menu.usernameSettingsShow();
		} 
		
		// В этом обработчике осуществляем все необходимые преобразования с графикой
		private function enterFrameListener(e:Event):void
		{	
			if (!connector.connected || menu.onStage)
				return;
			// если мы только что присоединились к группе, в которой есть люди,
			// то мяч ещё не создан - он создаётся после получения первой информации от хостов
			if (ball == null || scoreField == null || timer == null)
			{	
				if (connector.localGroup.neighborCount == 0)
				{
					// cheat - ждём 50 первых фреймов, т.е. 1 секунду прежде чем добавить мяч на сцену, если соседей не нашлось
					// Если мы первые в группе, то просто создаём сцену с мячём в середине поля
					ball = new Ball(0, 0);
					ball.setBall();
					addChild(ball);

					// создаём поле со счётом
					scoreField = new ScoreField();
					allField.addChild(scoreField);
					
					// создаём таймер обратного отсчёта
					timer = new TimerField();
					allField.addChild(timer);
				}
				connector.sendAll( { "x" : player._x, "y" : player._y, "sx" : player.speedX, "sy" : player.speedY } );
				return;
			}
			
			// Проверяем, не пересекаются ли мяч с игроком
			var dist:Number = Math.sqrt((ball._x - player._x) * (ball._x - player._x) + (player._y - ball._y) * (player._y - ball._y));
			var radiusSum:Number = Number(PlayerObject.PLAYER_RADIUS) + Number(Ball.BALL_RADIUS); 
			if (dist <= radiusSum)
				RoundObject.solveCircles(player, ball);
			
			// пересечение мяча и игрока со штангой
			for each (var bar:Bar in bars) 
			{
				dist = (ball._x - bar._x) * (ball._x - bar._x) + (bar._y - ball._y) * (bar._y - ball._y);
				radiusSum = (Number(Ball.BALL_RADIUS) + Number(Bar.BAR_RADIUS)) * (Number(Ball.BALL_RADIUS) + Number(Bar.BAR_RADIUS));
				if (dist <= radiusSum)
					ball.strikeBar(bar);
				dist = (player._x - bar._x) * (player._x - bar._x) + (bar._y - player._y) * (bar._y - player._y);
				radiusSum = (PlayerObject.PLAYER_RADIUS + Bar.BAR_RADIUS) * (PlayerObject.PLAYER_RADIUS + Bar.BAR_RADIUS);
				if (dist <= radiusSum)
					player.strikeBar(bar);
				
			}
			
			// пересечение остальных игроков с мячем, штангами и нашим игорком
			for each (var item:OtherPlayerObject in others) 
			{
				dist = Math.sqrt((ball._x - item._x) * (ball._x - item._x) + (item._y - ball._y) * (item._y - ball._y));
				radiusSum = Number(PlayerObject.PLAYER_RADIUS) + Number(Ball.BALL_RADIUS); 
				if (dist <= radiusSum)
					RoundObject.solveCircles(item, ball);
				dist = Math.sqrt((player._x - item._x) * (player._x - item._x) + (item._y - player._y) * (item._y - player._y));
				if (dist <= OtherPlayerObject.PLAYER_RADIUS * 2)
					RoundObject.solveCircles(player, item);
					
				// пересечение со штангами
				for each (bar in bars) 
				{
					dist = (item._x - bar._x) * (item._x - bar._x) + (bar._y - item._y) * (bar._y - item._y);
					radiusSum = (OtherPlayerObject.PLAYER_RADIUS + Bar.BAR_RADIUS) * (OtherPlayerObject.PLAYER_RADIUS + Bar.BAR_RADIUS);
					if (dist <= radiusSum)
						item.strikeBar(bar);
				}	
			}
			
			// Выполняем перемещение игрока в соответствии с направлением скорости
			player.move();
			// Перемещаем других игроков
			for each (var elem:OtherPlayerObject in others)  
			{
				elem.move();
			}
			
			ball.move();
			
			// отправляем данные о своём местоположении и скорости, если подключены к группе
			// делаем это через n кадров
			// если нам недавно пришло событие о том что кто-то пнул мяч, то втечение нескольких секунд не отправляем данные о мяче
			if (_counter % 2 == 0 )
				connector.sendAll( {"x" : player._x, "y" : player._y, "sx" : player.speedX, "sy" : player.speedY } );
			
			// отправляем данные о мяче, счёте и таймере, если не находимся в фазе сна после принятого сообщения о ключевом действии:
			if (!sleep && _counter % 10 == 0)
				connector.sendAll( { "bx" : ball._x, "by" : ball._y, "bsx" : ball.speedX, "bsy" : ball.speedY,
																	"score" : scoreField.text, "timer" : timer.text} );
			if (sleep)
			{
				if (sleepCounter == 200) // прошло 100 кадров(2 секудны с момента удара по мячу) - выключаем счётчик
					sleep = false;
				sleepCounter = (sleep) ? sleepCounter + 1 : 0;
			}
			
			_counter = (_counter == 29) ? 0 : _counter + 1; // прибавляем счётчик, если следующий кадр не корректировочный
		}
		
		
		
		private function keyDownListener(e:KeyboardEvent):void 
		{
			switch (e.keyCode)
			{
			case Keyboard.CONTROL:	// Удар по мячу
				kick();
				return;
			case Keyboard.DOWN:
			case Keyboard.S:
				player.isDown = true;
				break;
			case Keyboard.UP:
			case Keyboard.W:
				player.isUp = true;
				break;
			case Keyboard.RIGHT:
			case Keyboard.D:
				player.isRight = true;
				break;
			case Keyboard.LEFT:
			case Keyboard.A:
				player.isLeft = true;
				break;
			default:
				return;
			}
			player.addSpeed();
		}
		
		private function keyUpListener(e:KeyboardEvent):void 
		{
			switch (e.keyCode)
			{
			case Keyboard.DOWN:
			case Keyboard.S:
				player.isDown = false;
				break;
			case Keyboard.UP:
			case Keyboard.W:
				player.isUp = false;
				break;
			case Keyboard.RIGHT:
			case Keyboard.D:
				player.isRight = false;
				break;
			case Keyboard.LEFT:
			case Keyboard.A:
				player.isLeft = false;
				break;
			default:
				return;
			}
			player.decreaseSpeed();
		}

		// Удар по мячу
		private function kick():void 
		{
			if (player == null || ball == null)
				return;
			
			// расстояние между объектами
			var distX:Number = ball._x - player._x; 
			var distY:Number = ball._y - player._y;
			
			// расстояния между шарами
			var distance:Number = Math.sqrt ( distX * distX + distY * distY );
			
			if (! (distance - Ball.BALL_RADIUS <= PlayerObject.SPHERE_RADIUS ))
				return;
			
			// косинусы углов между линией, соединяющей центры окружностей,
			// и проекциями этой линии на оси координат
			var cosX:Number = distX/distance; 
			var sinX:Number = distY/distance;
			
			// придаём мячу скорость в направлении линии, соединяющей центры шаров
			// при этом учитываем расстояение между шарами: чем дальше - там слабже удар
			const MAX_BALL_SPEED:Number = 10;
			const MIN_BALL_SPEED:Number = 0.1;
			// функция скорости линейная: a*x + b
			var a:Number = (MAX_BALL_SPEED - MIN_BALL_SPEED) / (PlayerObject.PLAYER_RADIUS - PlayerObject.SPHERE_RADIUS); 
			var b:Number = MAX_BALL_SPEED - a * PlayerObject.PLAYER_RADIUS;
			var newSpeed:Number = a* (distance - Ball.BALL_RADIUS) + b;
			ball.speedX = cosX * newSpeed;
			ball.speedY = sinX * newSpeed;
			
			// отправляем данные о мяче
			sendKeyBall();
		}
		
	
		
		
		public function initGame():void 
		{
			player.setPlayer();
			// Добавляем игрока на сцену, добавляем обработчик события нажатия и отпускания клавиш
			VK.api("storage.get", { key : "username" }, function(result:Object):void
			{
				player.setUserName(String(result));
			}, 
			function(err:Object):void { } );
			this.addChild(player);
			
			//connector.testVKStorage();
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDownListener);
			stage.addEventListener(KeyboardEvent.KEY_UP, keyUpListener);
			stage.addEventListener(Event.ENTER_FRAME, enterFrameListener);
		}
		
		
		// отправляем данные о мяче всем соседям в ключевой момент, когда мы с ним производим какое-то действие
		private function sendKeyBall():void
		{
			connector.sendAll( { "bx" : ball._x, "by" : ball._y, "bsx" : ball.speedX, "bsy" : ball.speedY, "k"  : true} );
		}
		
		public function removeGroupFromVK(onRemoved:Function = null):void
		{
			if (connector.localGroup == null || connector.lobbyName == null)
				return;
			// Если группа была в списке доступных групп во вконтакте, то удаляем её
			VK.api("storage.get", { key : region.region_id, global : 1 }, function(list:String):void
			{
				var arr:Array = JSON.decode(list)["data"];
				// b. если в массиве есть наша группа, то избавляемся от неё
				var newArray:Array = arr.filter(function(item:Object, index:int, theArr:Array):Boolean
				{
					trace(connector.lobbyName);
					return item.id != connector.lobbyName;
				});
				// пишем новые данные, если в пришедшем массиве была группа, которую нужно удалить
				if (newArray.length < arr.length)
				{							
					VK.api("storage.set", { key :  region.region_id, value : JSON.encode( { "data" : newArray } ), global : 1 },
					function(result:int):void 
					{
						trace("Prev. group removed successfully.");
						if(onRemoved != null) onRemoved();
					}, function(error:Object):void {if(onRemoved != null) onRemoved(); } );
				}
				else
					if(onRemoved != null) onRemoved();
			},
			function(err:Object):void { } );
		}
		
		
		
		public function timeOver():void 
		{
			removeGroupFromVK();
			
			// Выходим из группы
			connector.localGroup.close();
			// убираем все остатки после игры
			resetGame();
			// Вызываем меню
			this.addChild(menu);
		}
		
		public function resetGame():void 
		{
			connector.connected = false;
			
			// удаляем счёт и таймер
			if (this.scoreField != null)
			{
				this.allField.removeChild(this.scoreField);
				this.scoreField = null;
			}
			if (this.timer != null)
			{
				this.allField.removeChild(this.timer);
				this.timer = null;
			}
			// удаляем мяч
			if (this.ball != null)
			{
				this.removeChild(this.ball);
				this.ball = null;
			}
			// убираем игроков с поля
			if (this.player != null)
			{
				if(this.player.stage != null)
					this.removeChild(this.player);
				this.player._x = 0;
				this.player._y = 0;
			}
			for each (var item:OtherPlayerObject in this.others) 
			{
				if (item.stage != null)
					this.removeChild(item);
			}
			// удаляем всех из списка others
			this.others = [];
			
		}
	}

}