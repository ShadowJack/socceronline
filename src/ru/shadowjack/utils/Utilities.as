package ru.shadowjack.utils 
{
	/**
	 * ...
	 * @author ShadowJack
	 */
	
	import flash.utils.ByteArray
	import flash.events.*;
	import flash.utils.Timer;
	
	public class Utilities 
	{
		
		public function Utilities() 
		{}
		
		public static function clone(obj:Object):Object 
		{
			var temp:ByteArray = new ByteArray();
			temp.writeObject(obj);
			temp.position = 0;
			return temp.readObject();
		}
	}

}