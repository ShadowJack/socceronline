package ru.shadowjack.utils 
{
	/**
	 * ...
	 * @author ShadowJack
	 */

	import flash.events.Event;
	import flash.events.NetStatusEvent;
	import flash.events.TimerEvent;
	import flash.net.GroupSpecifier;
	import flash.net.NetConnection;
	import flash.net.NetGroup;
	import flash.utils.Timer;
	import ru.shadowjack.soccer.Ball;
	import ru.shadowjack.soccer.OtherPlayerObject;
	import ru.shadowjack.soccer.ScoreField;
	import ru.shadowjack.soccer.TimerField;
	import vk.api.serialization.json.JSON;
	
	public class ConnectionManager 
	{
		
		public static const SERVER:String = "rtmfp://p2p.rtmfp.net/";
		public static const DEVKEY:String = "82ebeab3a1ef34a2e322f4f0-031a8b110dd8";
		
		public var localGroup:NetGroup = null;
		public var lobbyName:String;
		public var connected:Boolean = false;
		
		private var _main:Main;
		private var _connection:NetConnection;
		private var _groupList:Array;
		private var _currRegionId:String;
		private var _timer:Timer;
		
		
		public function ConnectionManager(main:Main) 
		{
			_main = main;
		}
		
		public function sendAll(data:Object):void
		{
			if(localGroup)
				this.localGroup.sendToAllNeighbors(data);
		}
		
		private function onNetStatusHandler(event:NetStatusEvent):void
		{
			switch (event.info.code) // Если произошло событие подключения к циррусу 
			{
				case "NetConnection.Connect.Success":
					onCirrusConnect(event);
					break;
					
				case "NetConnection.Connect.Closed":
					if(lobbyName && localGroup && localGroup.neighborCount == 0)
						_main.removeGroupFromVK();
					connected = false;
					break;
					
				case "NetGroup.Connect.Success":
					// запускаем таймер, по истечении срока которого будет выполнена проверка на наличие соседей
					_timer = new Timer(500, 1);
					_timer.addEventListener(TimerEvent.TIMER, onConnectedToGroup);
					_timer.start();
					break;

				case "NetGroup.Neighbor.Connect":
					
					break;
					
				case "NetGroup.Neighbor.Disconnect":
					onPeerDisconnected(event);
					break;
					
				case "NetGroup.SendTo.Notify":
					trace(JSON.encode(event.info.message));
					if (event.info.fromLocal == false)
					{
						onGetMessageFromPeer(event);
					}
					break;
					
				default:
					trace("onGroupStatusHandler default trigger: " + event.info.code); 
			}
		}
		
		private function connectToNextGroup(regionId:String):void 
		{
			var group:Object = _groupList.pop();
			// добавляеем нового игрока в группу:
			lobbyName = group.id;
			group.num++;
			trace("Group member count: " + group.num);
			// если при добавлении нового игрока к группе она заполнилась, то удаляем её из массива
			// и записываем массив в переменную по-новой
			if (group.num < 8) // полная группа 8 человек
			{
				// возвращаем вынутую из списка группу на место
				_groupList.push(group);
			}
			// записываем список групп обратно на сервер
			_main.VK.api("storage.set", { key : regionId, value : JSON.encode( { "data" : _groupList } ), global : 1 }, function(result:int):void
			{
				trace("3.Set: " + regionId);
				if (!result)
					trace("Save error!");
			}, function(e:Object):void { } );
			// выводим в консоль приложения данные о группе
			//_main.console.text = _main.console.text.concat("Lobby name: ", lobbyName, "\rMembers: ", group.num, "\r");
			// подключаемся к серверу и выходим из функции
			_connection.connect(ConnectionManager.SERVER, ConnectionManager.DEVKEY);
		}
		
		private function getUsernameFromVK(player:OtherPlayerObject):void 
		{
			_main.VK.api("storage.get", { key : "username" }, function(result:Object):void
			{
				player.setUserName(String(result));
			}, 
			function(err:Object):void 
			{
				// Пробуем повторить запрос через некоторое время, если нам не удалось получить ник из-за проблем с VK
				trace("Error: " + err);
				getUsernameFromVK(player);
			} );
		}
		
		private function onCirrusConnect(event:NetStatusEvent):void 
		{
			// добавляем пользователя в список доступных пользователей в базу данных на сервере
			// регистрируем нового пользователя в общей группе
			var globalGroupSpecifier:GroupSpecifier = new GroupSpecifier("ru.shadowjack.socceronline." + lobbyName);
			
			globalGroupSpecifier.postingEnabled = false;
			globalGroupSpecifier.routingEnabled = true;
			globalGroupSpecifier.serverChannelEnabled = true;
			
			localGroup = new NetGroup(_connection, globalGroupSpecifier.groupspecWithoutAuthorizations());
			localGroup.addEventListener(NetStatusEvent.NET_STATUS, onNetStatusHandler );
		}
		
		private function onPeerDisconnected(event:NetStatusEvent):void 
		{
			if (_main.others[event.info.neighbor] != undefined)
			{
				_main.removeChild(_main.others[event.info.neighbor]);
				delete _main.others[event.info.neighbor];
				// записываем на сервер контакта информацию, о том, что группа не полная
				// а. получаем массив с группами
				if (_main.region == null)
				{
					trace("Region is null, something is wrong!");
					return;
				}
				_main.VK.api("storage.get", { key : _main.region.region_id, global : 1 }, function(list:String):void
				{
					trace("Get: " + _main.region.region_id);
					var arr:Array = JSON.decode(list)["data"];
					// b. если в массиве есть наша группа, то избавляемся от неё
					var newArray:Array = arr.filter(function(item:Object, index:int, theArr:Array):Boolean
					{
						return item.id != lobbyName;
					});
					var deletedElements:Array = [];
					// TODO: решить, что выгоднее - искать, удалённую группу и смотреть, 
					// количество человек в ней или не искать совсем и всегда писать на сервер новое значение
					
					// если новый массив отличается от пришедшего, значит в исходном была наша группа - находим её
					if (newArray.length < arr.length) 
					{
						deletedElements = arr.filter(function(item:Object, index:int, theArr:Array):Boolean
						{
							return item.id == lobbyName;
						});
					}
					// c. если нашли группу, с количеством человек превышающем актуальное
					// или не нашли такой группы вовсе, то пишем новые данные
					if ((deletedElements.length > 0 && deletedElements[0].num > (localGroup.neighborCount + 1)) || deletedElements.length == 0)
					{							
						newArray.push( { id : lobbyName, num : (localGroup.neighborCount + 1) } );
						_main.VK.api("storage.set", { key :  _main.region.region_id, value : JSON.encode( { "data" : newArray } ), global : 1 },
						function(result:int):void
						{
							trace("2.Set: " + _main.region.region_id);
							//_main.console.text = _main.console.text.concat("Lobby name: ", lobbyName, "\rMembers: ", (localGroup.neighborCount + 1), "\r");
						},
						function(error:Object):void{});
					}
				},
				function(err:Object):void { } );
			}
		}
		
		private function onGetMessageFromPeer(event:NetStatusEvent):void 
		{
			// пришло сообщение о команде какого-то игрока
			if (event.info.message.team != null)
			{
				// проверяем, есть ли этот игрок в списке подключившихся
				if (_main.others[event.info.from] == null)
				{
					var newPlayer:OtherPlayerObject = new OtherPlayerObject(0, 0, event.info.from,
													(_main.player.team == event.info.message.team));
					//получаем ник игрока и добавляем на экран
					getUsernameFromVK(newPlayer);
					
					_main.others[event.info.from] = newPlayer;
					
					// отправляем новому игроку инфу о своей команде
					sendAll({"team" : _main.player.team});
				}
				// проверяем, изменилась ли команда игрока(фича со сменой команды будет добавлена в будущем)
				else if(OtherPlayerObject(_main.others[event.info.from]).isTeammate != (_main.player.team == event.info.message.team)) 
					OtherPlayerObject(_main.others[event.info.from]).isTeammate = !OtherPlayerObject(_main.others[event.info.from]).isTeammate;
				return;
			}
			if ( event.info.message.score != null)// если пришли данные о счёте
			{
				if (_main.scoreField == null)
				{
					_main.scoreField = new ScoreField(event.info.message.score);
					_main.allField.addChild(_main.scoreField);
				}
				// сравниваем пришедшие данные с текущими, если на пришедших счёт больше наших, то обновляем наши данные
				else if(_main.scoreField.text < event.info.message.score)
				{
					_main.scoreField.updateField(event.info.message.score);
				}
			}
			if ( event.info.message.timer != null && _main.timer == null)// если пришли данные о времени
			{
				_main.timer = new TimerField(event.info.message.timer);
				_main.allField.addChild(_main.timer);
			}
			// пришло сообщение о мяче:
			if(event.info.message.bx != null)
			{
				if ( _main.ball == null ) // если мы ещё не создали объект мяч, то самое время его создать из полученных нами данных
				{
					_main.ball = new Ball(event.info.message.bx, event.info.message.by);
					_main.ball.speedX = event.info.message.bsx;
					_main.ball.speedY = event.info.message.bsy;
					_main.addChild(_main.ball);
					_main.sleep = (event.info.message.k == true);
				}
				else if (event.info.message.k == true)	// если прислали ключевое событие
				{
					_main.ball._x = event.info.message.bx;
					_main.ball._y = event.info.message.by;
					_main.ball.speedX = event.info.message.bsx;
					_main.ball.speedY = event.info.message.bsy;
					_main.sleep = true;
				}
				else
				{
					_main.ball._x -= (_main.ball._x - event.info.message.bx) * 0.8;
					_main.ball._y -= (_main.ball._y - event.info.message.by) * 0.8;
					_main.ball.speedX -= (_main.ball.speedX - event.info.message.bsx) * 0.8;
					_main.ball.speedY -= (_main.ball.speedY - event.info.message.bsy) * 0.8;
				}
			}
			// извлекаем новые значения координат и скорости объекта, отправившего сообщение
			// сообщения содеражат PeerId, newX, newY, newSpeedX, newSpeedY
			else if (_main.others[event.info.from] != null)
			{	
				var peer:OtherPlayerObject = _main.others[event.info.from];
				if (!peer.displayed) // если нам пришли данные соседа, который ещё не отображён на экране
				{
					peer._x = event.info.message.x;
					peer._y = event.info.message.y;
					_main.addChild(peer);
					peer.displayed = true;
				}
				else // сосед уже находится в списке отображения
				{
					//TODO: рассмотреть случай, когда игроки заходят в комнату одновременно и ни у кого нету мяча
					if(event.info.message.x != null) // пришли данные о местоположении соседа, то обновляем его положение на сцене
						peer.updateState(event.info.message.x, event.info.message.y, event.info.message.sx, event.info.message.sy);
				}
			}
		}
		
		public function onConnectedToGroup(e:TimerEvent):void 
		{
			if (localGroup.neighborCount == 0 )
			{
				if (_groupList.length > 1)
				{
					// удаляем эту группу с сервера и подключаемся к другой
					_groupList.pop();
					connectToNextGroup(_currRegionId);
				}
				else if(_groupList.length == 1)
				{
					// изменяем сведения о нашей группе
					_groupList[0].num = localGroup.neighborCount + 1;
					_main.VK.api("storage.set", { key : _main.region.region_id, value : JSON.encode( { "data" : _groupList } ), global : 1 }, function(result:int):void
					{
						trace("1.Set: " + _main.region.region_id);
						if (!result)
							trace("Save error!");
					}, function(e:Object):void { } );
					connected = true;
					// присваиваем игроку команду
					_main.player.team = Boolean((localGroup.neighborCount + 1) % 2);
					// Отправляем всем инфу о своей команде
					sendAll({"team" : _main.player.team});
					_main.initGame();
				}
			}
			else
			{
				connected = true;
				// присваиваем игроку команду
				_main.player.team = Boolean((localGroup.neighborCount + 1) % 2);
				// Отправляем всем инфу о своей команде
				sendAll({"team" : _main.player.team});
				_main.initGame();
			}
		}
		
		public function connectToLobby(data:Object):void
		{
			//запоминаем пришедшие данные
			_currRegionId = JSON.decode(String(data))["region"]["region_id"];
			// для каждого региона на сервере есть глобальная переменная, которая доступна для всех 
			// экземпляров приложения
			// Если такой переменной ещё нет на сервере, то оставляем значение переменной found равным false
			var found:Boolean = false;
			_main.VK.api("storage.get", { key : _currRegionId, global : 1 }, function(list:String):void
			{
				trace("Get: " + _currRegionId);
				// парсим вернувшуюся строку, как массив: {"data" : [elem1, elem2, elem3, ...]}
				// itemX имеет вид {id : String, num : int}
				_groupList = JSON.decode(list)["data"];
				if (_groupList.length == 0)	// если свободных груп нету, то возвращаемся из функции
					return;
				found = true;
				connectToNextGroup(_currRegionId);
				
			},
			function(e:Object):void
			{
				trace(e);
			});

			// создаём таймер для проверки, не пришли, ли данные от контакта в течение 1000 миллисекунд
			var timer:Timer = new Timer(100, 10);
			
			timer.addEventListener(TimerEvent.TIMER, function():void
			{
				if (found)// выставился флаг - останавливаем таймер и выходим из функции
				{
					timer.stop();
					timer = null;
					return;
				}
			});
			// если прошло положенное время ожидания и флаг не выставился:
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, function(ev:TimerEvent):void
			{
				// если на сервере ещё не было списка групп для этого региона
				// или список групп был пуст, то создаём группу в данном регионе и записываем её на сервер
				var firstElem:Array = [{id : _currRegionId + String(Math.random()), num : 1}]
				_main.VK.api("storage.set", { key : _currRegionId, value : JSON.encode( { "data" : firstElem } ), global : 1 }, function(result:int):void
				{
					trace("4.Set: " + _currRegionId);
					lobbyName = firstElem[0].id;
					_main.player.team = Boolean(1);
					if (!result)
						trace("Save error!");
					else
					{
						// выводим в консоль приложения данные о группе
						//_main.console.text = _main.console.text.concat("Lobby name: ", lobbyName, "\r");
						_connection.connect(ConnectionManager.SERVER, ConnectionManager.DEVKEY);
					}
				}, function(e:Object):void { } );
				timer = null;
			});
			timer.start();
		}
		
		
		public function startConnection():void 
		{
			_connection = new NetConnection();
			_connection.addEventListener(NetStatusEvent.NET_STATUS, onNetStatusHandler);
		
			_main.VK.api("storage.get", { key : "location" }, function(result:Object):void
			{
				_main.country = JSON.decode(String(result))["country"];
				_main.region = JSON.decode(String(result))["region"];
				
				connectToLobby(result);
			}, function(error:Object):void 
			{
				trace("Error: ", error);
			});
			
		}
		
		public function testVKStorage():void
		{
			//DEBUG: для отладки записи значения вконтакте
			_groupList[0].num = 4;
			trace("Write to VK: "  + _main.region.region_id + " : " + JSON.encode(_groupList));
			_main.VK.api("storage.set", { key : _main.region.region_id, value : JSON.encode( { "data" : _groupList } ), global : 1 }, function(result:int):void
			{
				if (!result)
					trace("Save error!");
			}, function(e:Object):void { } );
			_main.VK.api("storage.get", { key : _currRegionId, global : 1 }, function(list:String):void
			{
				trace("Read from VK: " + _currRegionId + " : " + list);
				trace();
			},
			function(e:Object):void
			{
				trace(e);
			});
		}
	}

}