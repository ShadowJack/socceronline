package ru.shadowjack.utils 
{
	/**
	 * ...
	 * @author ShadowJack
	 */
	
	import flash.external.ExternalInterface;
	
	public class ExternalInterfaceUtil 
	{
	
		public static function available():Boolean
		{
			return ExternalInterface.available;
		}
		
		public static function addExternalEventListener( qualifiedEventName:String, callback:Function, callBackAlias:String ):void
		{
			// 1. Регистрируем коллбэк в Javascript-е, чтобы он его вызывал, когда произойдёт событие qualifiedEventName
			ExternalInterface.addCallback( callBackAlias, callback );
			// 2. Построени скрипта для выполнения браузером
			var	jsExecuteCallBack:String = "document.getElementsByName('"+ExternalInterface.objectID+"')[0]."+callBackAlias+"()";
			var jsBindEvent:String = "function(){"+qualifiedEventName+"= function(){"+jsExecuteCallBack+"};}";
			// 3. Выполнение джаваскрипта, которые привяжет внешнее событие с внутренним коллбэком
			ExternalInterface.call( jsBindEvent );
		}
		
	}

}