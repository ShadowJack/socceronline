package ru.shadowjack.soccer 
{
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author ShadowJack
	 */
	public class ScoreField extends TextField 
	{
		private var score:int;
		
		public function ScoreField(scoreText:String = "0:0") 
		{
			super();
			score = scoreText.split(':', 2)[0]*10 + scoreText.split(':', 2)[1];
			this.type = TextFieldType.DYNAMIC;
			this.autoSize = TextFieldAutoSize.CENTER;
			this.selectable = false;
			this.defaultTextFormat = new TextFormat("Verdana", 25, 0xE2E2E2, true);
			this.text = scoreText;
			this.x = 400 - this.width / 2;
			this.y = -6;
		}
		
		public function updateField(scoreText:String):void
		{
			this.text = scoreText;
			score = scoreText.split(':', 2)[0]*10 + scoreText.split(':', 2)[1];
		}
		
		/**
		 * Левая команда забила гол 
		 **/
		 
		public function leftScored():void
		{
			score += 10;
			this.text = int(score / 10) + ":" + score % 10;
			Main(this.parent.parent).player.setPlayer();
			Main(this.parent.parent).ball.setBall();
		}
			
		/**
		 * Правая команда забила гол 
		 **/
		 
		public function rightScored():void
		{
			score++;
			this.text = int(score / 10) + ":" + (score % 10);
			Main(this.parent.parent).player.setPlayer();
			Main(this.parent.parent).ball.setBall();
		}
	}

}