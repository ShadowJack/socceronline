package ru.shadowjack.soccer 
{
	import flash.display.Sprite;
	
	public class RoundObject extends Sprite 
	{
		// Скорость круга и его координаты
		protected var _speedX:Number;
		protected var _speedY:Number;
		public var _x:Number;	// точные  
		public var _y:Number;	//			координаты
		
		// Масса объекта
		protected var _mass:Number;
		
		public function RoundObject(x:Number, y:Number):void
		{
			super();
			this.x = x;
			this.y = y;
			this._x = x;
			this._y = y;
			_speedX = 0;
			_speedY = 0;
		}
		
		// Обработка столкновений двух круглых объектов
		public static function solveCircles(first:RoundObject, second:RoundObject):void
		{  
			// расстояние между объектами
			var distX:Number = second._x - first._x; 
			var distY:Number = second._y - first._y;
			
			// проверяем на столкновение
			if (((first.speedX - second.speedX) * distX + (first.speedY - second.speedY) * distY) > 0 )
			{
				// расстояния между шарами
				var distance:Number = Math.sqrt ( distX * distX + distY * distY );
 
				// косинусы углов между линией, соединяющей центры окружностей,
				// и проекциями этой линии на оси координат
				var cosX:Number = distX/distance; 
				var sinX:Number = distY/distance;
				
				// поворачиваем векторы скорости объектов так, чтобы центры объектов лежали на оси X
				var turnedFirstVelX:Number = first.speedX * cosX - first.speedY * ( -sinX);
				var turnedFirstVelY:Number = first.speedX * (-sinX) + first.speedY * cosX;
				
				var turnedSecondVelX:Number = second.speedX * cosX - second.speedY * ( -sinX);
				var turnedSecondVelY:Number = second.speedX * ( -sinX) + second.speedY * cosX;
				
				// Вычисляем новые скорости - меняются только X-компоненты
				var newTurnedFirstVelX:Number = (( first.mass - second.mass ) * turnedFirstVelX + 
									2 * second.mass * turnedSecondVelX) / ( first.mass + second.mass );
				var newTurnedSecondVelX:Number = turnedFirstVelX + newTurnedFirstVelX - turnedSecondVelX;
				// обратный поворот:
				first.speedX = newTurnedFirstVelX*cosX - turnedFirstVelY*sinX;
				first.speedY = newTurnedFirstVelX*sinX + turnedFirstVelY*cosX;
				second.speedX = newTurnedSecondVelX*cosX - turnedSecondVelY*sinX;
				second.speedY = newTurnedSecondVelX * sinX + turnedSecondVelY * cosX;
				// располагаем объекты так, что бы они не пересекались:
				// 1. Вычисляем середину отрезка, соединяющего центры шаров
				var midX:Number = (first._x + second._x) / 2;
				var midY:Number = (first._y + second._y) / 2;
				// 2. Вычисляем радиусы шаров
				var firstR:Number = (first is PlayerObject || first is OtherPlayerObject) ? PlayerObject.PLAYER_RADIUS : Ball.BALL_RADIUS;
				var secondR:Number = (second is PlayerObject || second is OtherPlayerObject) ? PlayerObject.PLAYER_RADIUS : Ball.BALL_RADIUS;
				// 3. Вычисляем проекции радиусов шаров на оси
				var firstRx:Number = firstR * cosX;
				var secondRx:Number = secondR * cosX;
				var firstRy:Number = firstR * sinX;
				var secondRy:Number = secondR * sinX;
				// 4. Раздвигаем шары вдоль линии, соединяющей их центры, на почтительное расстояние
				first._x = midX - firstRx;
				first._y = midY - firstRy;
				second._x = midX + secondRx;
				second._y = midY + secondRy;
			}
		}
		
		public function strikeBar(bar:Bar):void 
		{
			// Перемещаем игрока так, чтобы он не пересекался со штангой
			// расстояние между объектами
			var distX:Number = this._x - bar._x; 
			var distY:Number = this._y - bar._y;

			// расстояния между шарами
			var distance:Number = Math.sqrt ( distX * distX + distY * distY );

			// косинусы углов между линией, соединяющей центры окружностей,
			// и проекциями этой линии на оси координат
			var cosX:Number = distX/distance; 
			var sinX:Number = distY/distance;
			
			// располагаем объекты так, что бы они не пересекались:
			// 1. Вычисляем точку касания игрока и штанги
			var pointX:Number = bar._x + Bar.BAR_RADIUS*cosX;
			var pointY:Number = bar._y + Bar.BAR_RADIUS*sinX;
			// 2. Отодвигаем игрока так, чтобы он касался штанги
			this._x = pointX + PlayerObject.PLAYER_RADIUS * cosX;
			this._y = pointY + PlayerObject.PLAYER_RADIUS * sinX;
		}
		
		public function get speedX():Number 
		{
			return _speedX;
		}
		
		public function set speedX(value:Number):void 
		{
			_speedX = value;
		}
		
		public function get speedY():Number 
		{
			return _speedY;
		}
		
		public function set speedY(value:Number):void 
		{
			_speedY = value;
		}
		
		public function get mass():Number 
		{
			return _mass;
		}
		
		public function move():void
		{
		}
	}

}