package ru.shadowjack.soccer 
{
	import flash.display.Shape;
	/**
	 * ...
	 * @author ShadowJack
	 */
	public class Bar extends RoundObject 
	{
		public static const BAR_RADIUS:Number = 4;
		
		private var shape:Shape;
		
		public function Bar(x:Number, y:Number) 
		{
			super(x, y);
			this._mass = Number.POSITIVE_INFINITY;
			shape =  new Shape();
			shape.graphics.beginFill(0xD6D6D6, 1);
			shape.graphics.drawCircle(0, 0, BAR_RADIUS);
			shape.graphics.endFill();
			addChild(shape);
		}
		
		public override function move():void
		{}
	}

}