package ru.shadowjack.soccer 
{
	import flash.display.Shape;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	/**
	 * ...
	 * @author ShadowJack
	 */
	public class OtherPlayerObject extends RoundObject 
	{
	
		public static const PLAYER_MASS:Number = 1;
		public static const PLAYER_RADIUS:Number = 16;
		public static const SPHERE_RADIUS:Number = 24;
		public var isTeammate:Boolean;
		public var displayed:Boolean = false;
		private var _peerID:String;
		private var sphere:Shape;
		private var player:Shape;
		private var userName:TextField;
		private var userNumber:TextField;
		
		
		
		public function OtherPlayerObject(x:Number, y:Number, peerID:String, teammate:Boolean = false) 
		{
			super(x, y);
			this._mass = PLAYER_MASS;
			this._peerID = peerID;
			this.isTeammate = teammate;
			// Создаём, рисуем и добавляем подложку
			sphere = new Shape();
			sphere.graphics.beginFill(0x6F6A48, 0.3);
			sphere.graphics.drawCircle(0, 0, SPHERE_RADIUS);
			sphere.graphics.endFill();
			this.addChild(sphere);
			
			player = new Shape();
			player.graphics.lineStyle(1);
			(isTeammate) ? player.graphics.beginFill(0xE1AC4A) : player.graphics.beginFill(0xE27449);
			// все взаимодействия с другими объектами осуществляются в кольце: PLAYER_RADIUS < r < внешний круг
			player.graphics.drawCircle(0, 0, PLAYER_RADIUS - 0.5);	// вычитаем 0.5 для линии
			player.graphics.endFill();
			this.addChild(player);
			
			// Создаём подпись с ником пользователя
			userName = new TextField();
			userName.defaultTextFormat = new TextFormat("Verdana", 12, 0x4A6F51, true);
			userName.selectable = false;
			userName.type = TextFieldType.DYNAMIC;
			userName.multiline = false;
			addChild(userName);
			
		}
		
		// Перемещение игрока по экрану на основании текущей скорости и координат
		public override function move():void
		{
			if (this.stage != null)
			{
				var r:Number = PLAYER_RADIUS;
				// Ограничиваем границы передвижения
				if (_x + r > stage.stageWidth) 
				{
					_x = stage.stageWidth - r;
					_speedX *= -1;		
				}
				if (_x - r < 0)
				{
					_x = r; 
					_speedX *= -1;
				}
				if (_y + r > stage.stageHeight)
				{
					_y = stage.stageHeight - r;
					_speedY *= -1;
				}
				if (_y < r)
				{
					_y = r;
					_speedY *= -1;
				}
				
				// обновляем значения скоростей и координат
				x = _x;
				y = _y;
				_x += _speedX;
				_y += _speedY;
			}
		}
		
		public function updateState(newX:Number, newY:Number, newSpeedX:Number, newSpeedY:Number):void 
		{
			// обновляем новые значения координат и скорости у этого соседа
			_x -= (_x - newX) * 0.8;
			_y -= (_y - newY) * 0.8;
			_speedX -= (_speedX - newSpeedX) * 0.8;
			_speedY -= (_speedY - newSpeedY) * 0.8;
		}
		
		// Присваивание ника пользователю
		public function setUserName(name:String):void
		{
			userName.text = name;
			userName.x = -(userName.textWidth / 2);
			userName.y = PLAYER_RADIUS + 2;
		}
		
		public function setTeammate(teammate:Boolean):void
		{
			this.isTeammate = teammate;
		}
		
		public function get peerID():String 
		{
			return _peerID;
		}
		
	}

}