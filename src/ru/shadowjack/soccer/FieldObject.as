package ru.shadowjack.soccer 
{
	/**
	 * ...
	 * @author ShadowJack
	 */
	
	/**
	 *  Класс содержит константы для использования 
	 *  при определении столкновений мяча и игроков 
	 *  с границами поля и воротами,
	 *  а также штанги - экземпляры класса RoungObject
	 */
	public class FieldObject 
	{
		public static const UPPER_BOUND:int = 25;
		public static const LOWER_BOUND:int = 475;
		public static const LEFT_BOUND:int = 45;
		public static const RIGHT_BOUND:int = 755;
		public static const UPPER_GOALS:int = 175;
		public static const LOWER_GOALS:int = 325;
		public static const LEFT_GOALS:int = 7;
		public static const RIGHT_GOALS:int = 793;
		
		public function FieldObject()
		{
		}
		
		/**
		 * @usage 	Сначала перед вызовом метода нужно добавить
		 * 			поле soccerField на главный экран
		 * @param	soccerField - графика для отображения поля
		 */
		public static function addBars(soccerField:Allfield):void
		{
			// создаём штанги и добавляем на поле
			var bar1:Bar = new Bar(LEFT_BOUND, UPPER_GOALS);
			var bar2:Bar = new Bar(LEFT_BOUND, LOWER_GOALS);
			var bar3:Bar = new Bar(RIGHT_BOUND, UPPER_GOALS);
			var bar4:Bar = new Bar(RIGHT_BOUND, LOWER_GOALS);
			Main(soccerField.parent).bars.push(bar1, bar2, bar3, bar4);
			soccerField.addChild(bar1);
			soccerField.addChild(bar2);
			soccerField.addChild(bar3);
			soccerField.addChild(bar4);
		}
		
	}

}