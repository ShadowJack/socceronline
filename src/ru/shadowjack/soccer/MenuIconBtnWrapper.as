package ru.shadowjack.soccer 
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author ShadowJack
	 */
	public class MenuIconBtnWrapper extends MenuIconBtn 
	{
		public var _menu:NewInteractiveMenu = null;
		private var _main:Main = null;
		
		public function MenuIconBtnWrapper() 
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(MouseEvent.CLICK, onClick);
		}
		
		private function onAdded(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			_main = Main(this.parent.parent);
			_menu = _main.menu;
		}
		
		private function onClick(e:MouseEvent):void 
		{
			if (_menu == null)
				_menu = _main.menu;
			// вызываем меню поверх всего нашего дела
			if (_menu != null && _main != null)
			{
				_main.addChild(_menu);
				_menu.onStage = true;
			}
		}
		
	}

}