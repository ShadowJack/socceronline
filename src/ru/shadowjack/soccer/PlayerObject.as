package ru.shadowjack.soccer 
{
	import flash.display.Shape;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;

	public class PlayerObject extends RoundObject 
	{
	
		// Максимум величины проекции скорости скорости
		public static const MAX_SPEED:Number = 1.7;
		public static const PLAYER_MASS:Number = 1;
		public static const PLAYER_RADIUS:Number = 16;
		public static const SPHERE_RADIUS:Number = 24;
	
	
		public var team:Boolean;
		
		// Нажатые в данный момент клавиши управления игроком
		public var isUp:Boolean = false;
		public var isRight:Boolean = false;
		public var isDown:Boolean = false;
		public var isLeft:Boolean = false;
		
		private var userName:TextField;
		private var userNumber:TextField;
		private var sphere:Shape;
		private var player:Shape;
		
		public function PlayerObject(x:Number, y:Number):void 
		{
			super(x, y);
			this._mass = PLAYER_MASS;
			this.team = true;
			// Создаём, рисуем и добавляем подложку
			sphere = new Shape();
			sphere.graphics.beginFill(0x6F6A48, 0.3);
			sphere.graphics.drawCircle(0, 0, SPHERE_RADIUS);
			sphere.graphics.endFill();
			addChild(sphere);
			
			player = new Shape();
			player.graphics.lineStyle(1);
			player.graphics.beginFill(0xE1AC4A);
			// все взаимодействия с другими объектами осуществляются в кольце: PLAYER_RADIUS < r < внешний круг
			player.graphics.drawCircle(0, 0, PLAYER_RADIUS - 0.5);	// вычитаем 0.5 для линии
			player.graphics.endFill();
			addChild(player);
			
			// Создаём подпись с ником пользователя
			userName = new TextField();
			userName.defaultTextFormat = new TextFormat("Verdana", 12, 0x4A6F51, true);
			userName.selectable = false;
			userName.type = TextFieldType.DYNAMIC;
			userName.multiline = false;
			addChild(userName);
			
		}
		
		/**
		 * @usage Метод расставляет игроков по полю
		 */
		public function setPlayer():void 
		{
			// Постановка игрока, который был уже создан, на определённое ему место
			// а) В зависимости от команды выбираем, половину поля игока
			if (team)// левая команда
			{
				// находим середину левой половины по Х и ставим туда игрока
				_x = 0.75 * FieldObject.LEFT_BOUND + 0.25 * FieldObject.RIGHT_BOUND; 
				// задаём произвольное значение Y
				_y = Math.round(Math.random() * (FieldObject.LOWER_BOUND - FieldObject.UPPER_BOUND)) + FieldObject.UPPER_BOUND;
			}
			else
			{
				_x = 0.25 * FieldObject.LEFT_BOUND + 0.75 * FieldObject.RIGHT_BOUND;
				_y = Math.round(Math.random() * (FieldObject.LOWER_BOUND - FieldObject.UPPER_BOUND)) + FieldObject.UPPER_BOUND;
			}
			speedX = 0;
			speedY = 0;
			x = _x;
			y = _y;
		}
		
		// Перемещение игрока по экрану на основании текущей скорости и координат
		public override function move():void
		{
			if (this.stage != null)
			{
				var r:Number = PLAYER_RADIUS;
				// Ограничиваем границы передвижения
				if (_x + r > stage.stageWidth) 
				{
					_x = stage.stageWidth - r;
					_speedX = (isRight) ? 0 : _speedX * -1;		
				}
				if (_x - r < 0)
				{
					_x = r; 
					_speedX = (isLeft) ? 0 : _speedX * -1;
				}
				if (_y + r > stage.stageHeight)
				{
					_y = stage.stageHeight - r;
					_speedY = (isDown) ? 0 : _speedY * -1;
				}
				if (_y < r)
				{
					_y = r;
					_speedY = (isUp) ? 0 : _speedY * -1;
				}
				
				x = _x;
				y = _y;
				// проверяем, нажата ли какая-нибудь клавиша,
				// если да, то проводим операции по изменению скорости
				if (isDown || isLeft || isRight || isUp)
					addSpeed();
				// если нет, то постепенно останавливаемся
				else
				decreaseSpeed();
				_x += _speedX;
				_y += _speedY;
			}
		}
		
		// Изменение скорости в соответствии с нажатыми клавишами
		public function addSpeed():void
		{
			// Прибавляем скорость движения вправо
			if (isRight == true)
				_speedX = MAX_SPEED;
			
			// Прибавляем скорость движения влево
			if (isLeft == true)
				_speedX = - MAX_SPEED;
			
			// Прибавляем скорость движения вверх
			if (isUp == true)
				_speedY = - MAX_SPEED;
			
			// Прибавляем скорость движения вниз
			if (isDown == true)
				_speedY = MAX_SPEED;
			
			// Если скорость привышает максимум - выравниваем компонены
			if ((Number(isRight) - Number(isLeft))*(Number(isDown) - Number(isUp)) != 0)
			{
				_speedX /= Math.sqrt(2);
				_speedY /= Math.sqrt(2);
			}
		}
		
		// Изменение скорости в соответствии с нажатыми клавишами
		public function decreaseSpeed():void
		{		
			if (isRight == false && isLeft == false)
			{
				_speedX = 0;
			}
			
			if (isUp == false && isDown == false)
			{
				_speedY = 0;
			}
		}
		
		// Присваивание ника пользователю
		public function setUserName(name:String):void
		{
			userName.text = name;
			userName.x = -(userName.textWidth / 2);
			userName.y = PLAYER_RADIUS + 2;
		}
		
	}

}