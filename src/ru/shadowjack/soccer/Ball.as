package ru.shadowjack.soccer 
{
	import ru.shadowjack.soccer.FieldObject;

	public final class Ball extends RoundObject 
	{
		// Константа, которая содержит в себе коэфициент трения
		// он учитывается при рассчёте скорости движения мяча
		public static const TENSION_COEFF:Number = 0.95;
		private static const BALL_MASS:Number = 1;
		public static const BALL_RADIUS:Number = 14;

		public function Ball(x:Number, y:Number):void
		{
			super(x, y);
			this._mass = BALL_MASS;
			this.graphics.lineStyle(1);
			this.graphics.beginFill(0xFAEC70);
			this.graphics.drawCircle(0, 0, BALL_RADIUS - 0.5);
			this.graphics.endFill();
		}
		
		// постановка мяча на середину поля, даже если он не добавлен на сцену
		public function setBall():void 
		{
			_x = (FieldObject.LEFT_BOUND + FieldObject.RIGHT_BOUND) / 2;
			_y = (FieldObject.UPPER_BOUND+ FieldObject.LOWER_BOUND) / 2;
			speedX = 0;
			speedY = 0;
			x = _x;
			y = _y;
		}
		
		override public function strikeBar(bar:Bar):void 
		{
			// Перемещаем игрока так, чтобы он не пересекался со штангой
			// расстояние между объектами
			var distX:Number = this._x - bar._x; 
			var distY:Number = this._y - bar._y;

			// расстояния между шарами
			var distance:Number = Math.sqrt ( distX * distX + distY * distY );

			// косинусы углов между линией, соединяющей центры окружностей,
			// и проекциями этой линии на оси координат
			var cosX:Number = distX/distance; 
			var sinX:Number = distY/distance;
			
			// поворачиваем вектор скорости мяча так, чтобы центры объектов лежали на оси X
			var turnedVelX:Number = this.speedX * cosX - this.speedY * ( -sinX);
			var turnedVelY:Number = this.speedX * (-sinX) + this.speedY * cosX;
			
			// Вычисляем новые скорости - меняются только X-компоненты
			turnedVelX *= -1;
			// обратный поворот:
			this.speedX = turnedVelX * cosX - turnedVelY * sinX;
			this.speedY = turnedVelX * sinX + turnedVelY * cosX;
			
			// располагаем объекты так, что бы они не пересекались:
			// 1. Вычисляем точку касания игрока и штанги
			var pointX:Number = bar._x + Bar.BAR_RADIUS*cosX;
			var pointY:Number = bar._y + Bar.BAR_RADIUS*sinX;
			// 2. Отодвигаем игрока так, чтобы он касался штанги
			this._x = pointX + Ball.BALL_RADIUS * cosX;
			this._y = pointY + Ball.BALL_RADIUS * sinX;
			
		}
		
		override public function move():void
		{
			if (this.stage != null)
			{
				x = _x;
				y = _y;
				var r:Number = BALL_RADIUS; 
				// центр мяча в воротах
				if (_x > FieldObject.RIGHT_BOUND || _x < FieldObject.LEFT_BOUND)
				{
					if (_x - r > FieldObject.RIGHT_BOUND) // забили гол в правые ворота
					{
						Main(this.parent).scoreField.leftScored();
						
					}
					else if (_x + r < FieldObject.LEFT_BOUND ) // забили в левые ворота
					{
						//trace("Иерархия отображения для мяча:\n" + this.parent + "\n" + this.parent.parent);
						Main(this.parent).scoreField.rightScored();
					}
					
					if (_y - r <= FieldObject.UPPER_GOALS)
					{
						_y = FieldObject.UPPER_GOALS + r;
						_speedY *= -1;
					}
					if (_y + r >= FieldObject.LOWER_GOALS)
					{
						_y = FieldObject.LOWER_GOALS - r;
						_speedY *= -1;
					}
					if (_x - r <= FieldObject.LEFT_GOALS)
					{
						_x = FieldObject.LEFT_GOALS + r;
						_speedX *= -1;
					}
					if (_x + r >= FieldObject.RIGHT_GOALS)
					{
						_x = FieldObject.RIGHT_GOALS - r;
						_speedX *= -1;
					}
				}
				else
				{
					// Случай, когда мяч лежит в пределах ворот
					if (_y - r >= FieldObject.UPPER_GOALS && _y + r < FieldObject.LOWER_GOALS)
					{	
						if (_x + r > FieldObject.RIGHT_GOALS) 
						{
							_x = FieldObject.RIGHT_GOALS - r;
							_speedX *= -1;
						}
						if (_x - r < FieldObject.LEFT_GOALS)
						{
							_x = FieldObject.LEFT_GOALS + r; 
							_speedX *= -1;
						}
					}
					else // мяч не в зоне ворот
					{
						if (_x + r > FieldObject.RIGHT_BOUND) 
						{
							_x = FieldObject.RIGHT_BOUND - r;
							_speedX *= -1;
						}
						if (_x - r < FieldObject.LEFT_BOUND)
						{
							_x = FieldObject.LEFT_BOUND + r; 
							_speedX *= -1;
						}
					}
					// если мяч в воротах, то нужно правильно обработать соударение с боковыми стенками
					if (_x - r < FieldObject.LEFT_BOUND || _x + r > FieldObject.RIGHT_BOUND)
					{
						if (_y + r > FieldObject.LOWER_GOALS)
						{
							_y = FieldObject.LOWER_GOALS - r;
							_speedY *= -1;
						}
						if (_y - r < FieldObject.UPPER_GOALS)
						{
							_y = FieldObject.UPPER_GOALS + r;
							_speedY *= -1;
						}
					}
					else
					{
						if (_y + r > FieldObject.LOWER_BOUND)
						{
							_y = FieldObject.LOWER_BOUND - r;
							_speedY *= -1;
						}
						if (_y - r < FieldObject.UPPER_BOUND)
						{
							_y = FieldObject.UPPER_BOUND + r;
							_speedY *= -1;
						}
					}
				}
				_x += _speedX;
				_y += _speedY;
				_speedX = (_speedX > 0.1 || _speedX < -0.1) ? _speedX * TENSION_COEFF : 0;
				_speedY = (_speedY > 0.1 || _speedY < -0.1) ? _speedY * TENSION_COEFF : 0;
			}
		}
		
	}

}