package ru.shadowjack.soccer 
{
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.utils.Timer;
	
	/**
	 * ...
	 * @author ShadowJack
	 */
	public class TimerField extends TextField 
	{
		
		private var timer:Timer;
		
		public function TimerField(initTime:String = "05:00") 
		{
			super();
			this.defaultTextFormat = new TextFormat("Verdana", 20, 0x4A6F51, true);
			this.text = initTime;
			this.type = TextFieldType.DYNAMIC;
			this.autoSize = TextFieldAutoSize.CENTER;
			this.selectable = false;
			this.x = 100 - this.width / 2;
			this.y = -3;
			
			timer = new Timer(1000);
			timer.addEventListener(TimerEvent.TIMER, function(e:TimerEvent):void
			{
				var sec:int = (text.charCodeAt(3) - "0".charCodeAt(0)) * 10 + text.charCodeAt(4) - "0".charCodeAt(0);
				var min:int = text.charCodeAt(1) - "0".charCodeAt(0);
				if (min == sec && sec == 0) // если время истекло
				{
					timer.stop();
					Main(parent.parent).timeOver();
				}
				else
				{
					sec = (sec == 0) ? 59 : sec - 1;
					min = (sec == 59) ? min - 1 : min;
					text = "0" + min + ":" + ((sec < 10) ? "0" + sec : sec);
				}
			});
			timer.start();
		}
		
	}

}