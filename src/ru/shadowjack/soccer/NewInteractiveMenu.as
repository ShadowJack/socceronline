package ru.shadowjack.soccer 
{
	import fl.controls.Button;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import field_fla.AltMenu_4;
	import CloseMenuBtn;
	import vk.api.serialization.json.*;
	
	/**
	 * ...
	 * @author ShadowJack
	 */
	public class NewInteractiveMenu extends AltMenu_4 
	{
		public var onStage:Boolean = false;
		
		private var _main:Main;
		
		public function NewInteractiveMenu() 
		{
			super();
			this.stop();
			addEventListener(Event.ADDED_TO_STAGE, addedListener);
			addEventListener(Event.REMOVED_FROM_STAGE, removedListener);
			
		}
		
		private function addedListener(e:Event):void 
		{
			onStage = true;
			_main = Main(this.parent);
			// отключаем кнопку меню
			_main.menuIcon.enabled = false;
			
			listenToMenu();
		}
		
		private function friendsPlay(e:MouseEvent):void 
		{
			
		}
		
		private function randomPlay(e:MouseEvent):void
		{
			// убираем меню с экрана
			_main.removeChild(this);
			// включаем кнопку меню
			_main.menuIcon.enabled = true;
			// проверяем, не находится ли наш клиент в уже действующей игре
			if (_main.connector.connected || _main.connector.localGroup)
			{
				// если кроме нас в группе никого нет, то удаляем её из контакта
				if (_main.connector.localGroup.neighborCount == 0)
				{
					_main.removeGroupFromVK(function():void
					{
						_main.connector.localGroup.close();
						_main.resetGame();
						_main.connector.startConnection();
					});
				}
				else
					_main.connector.startConnection();
			}
			else
				// Запускаем игру
				_main.connector.startConnection();	
		}
		
		public function usernameSettingsShow():void
		{
			// переключаем меню в режим ввода ника
			this.gotoAndStop(3);
			//_main.console.appendText("Вывод меню с выбором ника\r");
			this.usernameOKBtn.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):void
			{
				//_main.console.appendText("Сохранение ника\r");
				if (usernameField.text != null && usernameField.text != "")
				{
					_main.VK.api("storage.set", { key : "username", value : usernameField.text }, function(res:Object):void{}, function(e:Object):void{} );
				}
				settingsShow();
			});
		}
		
		public function settingsShow(e:MouseEvent = null):void 
		{
			// переключаем меню в режим настроек
			this.gotoAndStop(2);
			// получаем данные о выбранных настройках с сервера
			_main.VK.api("storage.get", { key : "location" }, function(location:Object):void
			{
				_main.country = JSON.decode(String(location))["country"];
				_main.region = JSON.decode(String(location))["region"];
				//trace(_main.country["label"] + " - " + _main.region.title);
			},
			function(error:Object):void { trace(error); } );
			// добавляем страны в список стран
			_main.VK.api("places.getCountries", { }, function(countries:Array):void
			{
				for each (var item:Object in countries) 
				{	
					//trace(item);
					item['label'] = item['title'];
					countryBox.addItem(item);
				}
				// если у нас есть предыдущие настройки, то ставим их
				if (_main.country != null && _main.country.index != null)
				{
					countryBox.selectedIndex = _main.country.index;
					onCountrySelected();
				}
			},
			function(error:Object):void
			{
				trace(error);
			});
			countryBox.addEventListener(Event.CHANGE, onCountrySelected);
			okBtn.addEventListener(MouseEvent.CLICK, onOkClicked);
			
		}
		
		private function closeMenu(e:MouseEvent):void 
		{
			// убираем меню с экрана
			_main.removeChild(this);
			// переключаем меню на первый кадр, чтобы при повторном вызове не возникало ошибок
			this.gotoAndStop(1);
			// включаем кнопку меню
			_main.menuIcon.enabled = true;
		
		}
		
		private function onCountrySelected(e:Event = null):void
		{
			// Очищаем список регионов
			regionBox.dataProvider.removeAll();
			// выполняем запрос к апи - получаем список регионов
			_main.VK.api("places.getRegions", { "country" : countryBox.selectedItem.cid }, function(regions:Array):void
			{
				for each (var reg:Object in regions) 
				{
					reg['label'] = reg['title'];
					regionBox.addItem(reg);
				}
				// если нами был выбран какой-то регион, то переключаемся на него
				if (_main.region != null && _main.region.index != null)
				{
					regionBox.selectedIndex = _main.region.index;
				}
			},
			function(error:Object):void { trace(error); } );
		}
		
		private function onOkClicked(e:MouseEvent):void 
		{
			// получаем значения выбранные в комбобоксах
			var cnt:Object = countryBox.selectedItem;
			cnt["index"] = countryBox.selectedIndex;
			var reg:Object = regionBox.selectedItem;
			reg["index"] = regionBox.selectedIndex;
			if (cnt == null || reg == null)
				return;
			// записываем в переменную на сервере
			_main.VK.api("storage.set", { key : "location", value : JSON.encode( { "country" : cnt, "region" : reg } ) }, function(result:Number):void
			{
				if (!result)
					trace("Saving error");
			},
			function(error:Object):void { trace("Saving error"); } );
			// переходим на предыдущий кадр меню
			this.prevFrame();
			listenToMenu();
		}
		
		private function removedListener(e:Event):void 
		{
			onStage = false;
		}
		
		private function listenToMenu():void 
		{
			// Добавляем прослушивание нажатий на кнопки
			friendsBtn.addEventListener(MouseEvent.CLICK, friendsPlay);
			randomBtn.addEventListener(MouseEvent.CLICK, randomPlay);
			settingsBtn.addEventListener(MouseEvent.CLICK, settingsShow);
			CloseMenuObj.addEventListener(MouseEvent.CLICK, closeMenu);
		}
		
		
	}

}